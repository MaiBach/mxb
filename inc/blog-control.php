<?php
if(!class_exists('TwentyTwentyOne_BlogController'))
{
    class TwentyTwentyOne_BlogController{

        static function _init()
        {
            
            add_action('init',array(__CLASS__,'_add_post_type'));
            
        }

        static function _add_post_type()
        {
            $labels = array(
                'name'               => esc_html__('Blog','twentytwentyone'),
                'singular_name'      => esc_html__('Blog','twentytwentyone'),
                'menu_name'          => esc_html__('Blog','twentytwentyone'),
                'name_admin_bar'     => esc_html__('Blog','twentytwentyone'),
                'add_new'            => esc_html__('Add New','twentytwentyone'),
                'add_new_item'       => esc_html__( 'Add New Blog','twentytwentyone' ),
                'new_item'           => esc_html__( 'New Blog', 'twentytwentyone' ),
                'edit_item'          => esc_html__( 'Edit Blog', 'twentytwentyone' ),
                'view_item'          => esc_html__( 'View Blog', 'twentytwentyone' ),
                'all_items'          => esc_html__( 'All Blog', 'twentytwentyone' ),
                'search_items'       => esc_html__( 'Search Blog', 'twentytwentyone' ),
                'parent_item_colon'  => esc_html__( 'Parent Blog:', 'twentytwentyone' ),
                'not_found'          => esc_html__( 'No Blog found.', 'twentytwentyone' ),
                'not_found_in_trash' => esc_html__( 'No Blog found in Trash.', 'twentytwentyone' )
            );

            $args = array(
                'labels'             => $labels,
                'public'             => true,
                'publicly_queryable' => true,
                'show_ui'            => true,
                'show_in_menu'       => true,
                'query_var'          => true,
                'rewrite'            => array( 'slug' => 'blog' ),
                'capability_type'    => 'post',
                'has_archive'        => true,
                'hierarchical'       => false,
                'menu_position'      => null,
                'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
            );

            register_post_type('blog',$args);
            self::twenty_twenty_one_add_custom_taxonomy();
        }

        static function twenty_twenty_one_add_custom_taxonomy (){
            register_taxonomy(
                'blog_category',
                'blog',
                array(
                    'label' => esc_html__( 'Blog Category', 'twentytwentyone' ),
                    'rewrite' => array( 'slug' => 'blog_category', 'twentytwentyone' ),
                    'hierarchical' => true,
                    'query_var'  => true
                )
            );
        }
    }

    TwentyTwentyOne_BlogController::_init();

}